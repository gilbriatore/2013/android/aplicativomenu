package br.up.edu.appmenu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void abrirExe01(View v){
        Intent intent = new Intent(this,Exe01Activity.class);
        startActivity(intent);
    }

    public void abrirExe02(View v){
        Intent intent = new Intent(this,Exe02Activity.class);
        startActivity(intent);
    }

}
